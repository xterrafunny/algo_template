FROM ubuntu:20.04

RUN apt-get update

# Install gcc
RUN apt-get install -y build-essential

# Install latest CMake
RUN apt-get install -y clang-format-12

COPY .clang-format .